# Generated by Django 3.0.4 on 2020-03-28 05:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pybackend', '0006_productlist'),
    ]

    operations = [
        migrations.CreateModel(
            name='CartList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cartprname', models.TextField()),
                ('cartprpice', models.IntegerField()),
                ('cartprcount', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='productlist',
            name='productdetal',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
