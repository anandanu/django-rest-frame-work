from django.db import models

# Create your models here.

class User(models.Model):
        first_name = models.TextField()
        email = models.EmailField()
        password1 = models.CharField(max_length=10)
        password2 = models.CharField(max_length=10)
        
class basicdetails(models.Model):
        name= models.TextField()
        age= models.TextField()
        details= models.TextField()

class authors(models.Model):
        authuorname= models.TextField()
        authordetails = models.CharField(max_length=100)

        def __str__(self):
                return self.authuorname


class albumlist(models.Model):
        albums_list= models.ForeignKey( authors , on_delete=models.CASCADE , blank=True , null = True)
        albumname = models.CharField(max_length=100)
        authorname = models.CharField(max_length=100)
        albumimage = models.ImageField(blank=True)
        album = models.FileField(blank=True)
        albumdetails = models.CharField(max_length=100)

        # def __str__(self):
        #         return '{} by {}'.format(self.albums_list, self.albumname)

class ProductList(models.Model):
        productimage= models.ImageField()
        productname = models.TextField()
        productprice = models.IntegerField()
        specialprice = models.IntegerField()
        productdetal = models.CharField(max_length =100, blank=True, null=True)

class CartList(models.Model):
        cartprname = models.TextField()
        cartprpice = models.IntegerField()
        cartprcount = models.IntegerField()
        carttotal = models.IntegerField(blank=True , null=True)


        