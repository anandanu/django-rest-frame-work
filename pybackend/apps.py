from django.apps import AppConfig


class PybackendConfig(AppConfig):
    name = 'pybackend'
