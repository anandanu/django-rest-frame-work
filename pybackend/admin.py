from django.contrib import admin
from .models import basicdetails
from .models import albumlist
from  .models import authors
from .models import ProductList
# Register your models here.

admin.site.register(authors)
admin.site.register(ProductList)

class basedetails(admin.ModelAdmin):
    list_details = ('name','age','details')
admin.site.register(basicdetails , basedetails)

class albumList(admin.ModelAdmin):
    album_list = ('albumname','authorname','albumimage', 'album' , 'albumdetails')
admin.site.register(albumlist , albumList)

class authour(admin.ModelAdmin):
    pass
    
