from django.shortcuts import render
from rest_framework import viewsets 
from django.shortcuts import get_object_or_404
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView
from rest_framework.response import Response
from .serializes import  basicserializer
from .serializes import albumSerializer
from .serializes import ProductSerializer
from .serializes import CartSerializers
from rest_framework import generics
from pybackend import models
from pybackend import serializes
from .serializes import UserSerializer
from .models import basicdetails
from rest_framework.decorators import api_view
from .models import albumlist
from  .models import authors
from .models import ProductList
from  .models import CartList
from django.contrib.auth.views import LoginView
from django.contrib.auth.models import User
from django.contrib.auth import login as auth_login
from django.contrib.auth.forms import UserCreationForm
from builtins import classmethod

from rest_framework.views import APIView


class LogView(generics.ListCreateAPIView):
     
    def get(self, request, format=None):
        snippets = User.objects.all()
        return Response(snippets)
     
 
    
class UserCreate(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
   
    def post(self , request):
        serializers = UserSerializer(data = request.data)
        return Response('Successfully Created')

        
class LoginView(viewsets.ViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    def get(self, request, *args, **kwargs):
        return request.user
    
class UserView(viewsets.ViewSet):
    serializer_class = UserSerializer
    def list(self , request):
        all_user = User.objects.all()
        print(all_user)
        serializer = UserSerializer(all_user)
        return Response(serializer.data)
        
    def create(self , request):
        userform = UserCreationForm(request.data)
        print(userform.is_valid())
        if userform.is_valid():
            saveing = userform.save()
            auth_login(request, saveing)
        return Response('Hello')
    
class ProductListView(viewsets.ViewSet):
    serializer_class = ProductSerializer 
    
    def get_queryset(self):
        all_products = ProductList.objects.all()
        return all_products
    
    def list(self , request):
        all_products = self.get_queryset()
        serializer = ProductSerializer(all_products , many=True)
        return Response(serializer.data)
    
    def create(self ,request):
        print(request.data)
       
    

class CartView(viewsets.ViewSet):

    def list(self , reques):
        all_products = CartList.objects.all()
        print(all_products)
        serializer = CartSerializers(all_products , many=True)
        return Response(serializer.data)
        
    def create(self , request):
        print('alert')
        id = request.data['id']
        products = ProductList.objects.filter(id=id)    
        carpr = [{'cartprname':productslist.productname , 'cartprpice':productslist.productprice ,'cartprcount':1} for productslist in products]
        for dict_set in carpr:
            serializer = CartSerializers(data=dict_set)
            print(serializer.is_valid())
            if serializer.is_valid():
                serializer.save()
        return Response(request.data)
    
    def destroy(self, request, pk=None):
        #print(pk)
        instancs = CartList.objects.filter(id=pk)
        instancs.delete()
        return Response({"message":"Item Removed Successfully", "data":request.data})
    
    def update(self , request , pk=None):
            instancs = CartList.objects.get(id=pk)
            data = request.data
            for count in data:
                print(count['cartprcount'])
                instancs.cartprcount = count['cartprcount']
                instancs.save()
            return Response("Updated Successfully")

class BasicView(viewsets.ModelViewSet):    
  serializer_class = basicserializer           
  queryset = basicdetails.objects.all()

class albumsView(viewsets.ModelViewSet):
  serializer_class =  albumSerializer
  all_objects = albumlist.objects.all()
  author_objects= authors.objects.all()
  
  def get_queryset(self):
    albumget = albumlist.objects.all()
    return albumget
    
  def get(self , request , pk):
    albums = self.get_queryset()
    serializer = albumSerializer(albums)
    return Response(serializer.data)
    
  def post(self , request):
    serializers = albumSerializer(data = request.data ,files=request.FILES)
    return Response('Successfully Created')
  
  #queryset = [{'albumname':albums.albumname , 'authorname':albums.authorname , 'albumimage':albums.albumimage , 'album':albums.album , 'albumdetails':albums.albumdetails , 'albums_list':albums.albums_list} for  albums in all_objects]