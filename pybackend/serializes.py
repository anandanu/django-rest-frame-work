from rest_framework import serializers
from .models import basicdetails
from .models import albumlist
from .models import ProductList
from .models import CartList
#from .models import User
from django.contrib.auth.models import User


class loginSerializer(serializers.Serializer):
    def post(self , request):
        return request.data
        
class basicserializer(serializers.ModelSerializer):
    class Meta:
        model = basicdetails
        fields = ('name' , 'age' ,'details')

class albumSerializer(serializers.ModelSerializer):
    albumimage = serializers.ImageField(
        max_length=None, use_url=True,
    )
    class Meta:
        model = albumlist
        fields ='__all__'
        
class UserSerializer (serializers.ModelSerializer):
        class Meta:
            model = User
            fields = ('username', 'email','password')
            write_only_fields = ('password',)
        
        def create(self, validated_data):
            print(validated_data)
            user = User.objects.create_user(**validated_data)
            return user

 
        
class ProductSerializer(serializers.ModelSerializer):
    productimage = serializers.ImageField(
            max_length=None, use_url=True,
        )
    class Meta:
        model = ProductList
        fields ='__all__'

class CartSerializers(serializers.ModelSerializer):
    class Meta:
        model = CartList
        fields='__all__'

